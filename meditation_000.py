#!/usr/bin/env python3

# Meditators: @maxigas, @efkin.

# Goal: implement 'comp()' clojure function in python3.

# Solution
def comp(*fs):
    """
    Takes a set of functions and returns a fn that is the
    composition of those fns. The returned fn takes a variable number of args,
    applies the rightmost of fns to the args, the next fn (right-to-left) to
    the result, etc.

    # Checking for single argument
    >>> [comp(lambda x: x*10,lambda x: x+2, lambda x: x*3, lambda x: x+3)(x) for x in range(6)]
    [110, 140, 170, 200, 230, 260]

    # Checking for multiple arguments
    >>> a = lambda x: sum(x)     
    >>> b = lambda x, y, z: [x*1, y*2, z*3]    
    >>> comp(a, b)(1, 2, 3)
    14
    """
    if len(fs) == 1:
        return lambda *x: fs[0](*x)
    else:
        return lambda *x: fs[0](comp(*fs[1:])(*x))

# Oneliners
# ... with the recursion base case in the end.
# comp = lambda *f:len(f)>2 and lambda *x: f[0](comp(*f[1:])(*x)) or lambda *x: f[0](*x)
# ... with the recursion base case in the beginning.
# comp = lambda *f:len(f)==1 and lambda *x: f[0](*x) or return lambda *x: f[0](comp(*f[1:])(*x))

if __name__ == '__main__':
    import doctest
    doctest.testmod()
